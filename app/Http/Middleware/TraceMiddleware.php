<?php

namespace App\Http\Middleware;

use App\Libs\RequestEnv;
use Closure;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\JsonResponse;
use OpenTracing\Span;
use App\Libs\Facade\SpanFacade;
use App\Libs\Trace\TraceClient;

class TraceMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $span = null;
        $tracer = null;
        try {
            $traceId = $request->header(RequestEnv::TRACE_ID, gen_trace_id());
            RequestEnv::setTraceId($traceId, true);

            $tracer = TraceClient::getInstance()->getTracer();
            $span = SpanFacade::buildRequestSpan();

            /**
             * @var $response Response
             */
            $response = $next($request);

            if ($span instanceof Span) {
                SpanFacade::appendResponseToSpan((string)$response->getStatusCode(), $response->getContent());
            }
            return $response;
        } catch (\Throwable $t) {
            if ($span instanceof Span) {
                SpanFacade::appendExceptionToSpan($t);
            }

            throw $t;
        } finally {
            SpanFacade::finish($span);
            $tracer->flush();
        }
    }
}
